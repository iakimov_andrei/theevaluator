/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.theevaluator;

/**
 *
 * @author 1331079
 */
public class CustomStack implements StackInterface {
    
    String [] array;
    
    public CustomStack(){
        array = new String [0];
    }
    @Override
    public boolean add(String e) {
                String [] newArray = new String [array.length + 1];
        for(int i = 0; i < array.length; i++){
            newArray[i] = array[i];
        }
        newArray[newArray.length - 1] = e;
        array = newArray;
        return true;
    }

    @Override
    public String pop() {
        String [] newArray = new String [array.length - 1];
        String element = array[array.length - 1];
        for(int i = 0; i < newArray.length; i++){
            newArray[i] = array[i];
        }        
        array = newArray;
        return element;
    }

    @Override
    public int size() {
        return array.length;
    }
    
}
