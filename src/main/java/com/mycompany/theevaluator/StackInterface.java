/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.theevaluator;

/**
 *
 * @author 1331079
 */
interface StackInterface {
    boolean add(String e);
    String pop();
    int size(); 
}
