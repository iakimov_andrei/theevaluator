/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.theevaluator;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

/**
 *
 * @author andrei
 */
public class TheEvaluator {
    private String expression;
    private String postfixExpression;
    CustomStack stack;
    CustomQueue queue;
    
    public TheEvaluator(String expression)
    {
        this.expression = expression;
        postfixExpression = "";
        stack = new CustomStack();
        queue = new CustomQueue();
    }
     
    public CustomQueue toPostfix() throws WrongExpressionException
    {
        char [] exp = expression.toCharArray();
        for(char c : exp)
        {
            if(Character.isDigit(c)){
                queue.add(c + "");
            } else if(checkOperand(c)){
                if(stack.size() == 0){
                    stack.add(c + "");
                } else {
                    String operator = stack.pop();
                    char o = operator.toCharArray()[0];
                    if(checkGreater(c, o)){
                        stack.add(c + "");
                        queue.add(operator);
                    } else {
                        stack.add(operator);
                        stack.add(c + "");
                    }
                }
            }else if (c == ' '){
                continue;
            } else {
                throw new WrongExpressionException("Must contain numeric charactes only");
            }  
        }
        
        while(stack.size() != 0){
            queue.add(stack.pop());
        }
        
        return queue;
        /*
        String res = "";
        
        while(queue.size() != 0 ){
            res += queue.remove();
        }
        return res;
        */
    }
    
    private boolean checkOperand(char c)
    {
        if (c == '*' || c == '/' || c == '+' || c == '-')
            return true;
        else 
            return false;
    }
    
    //o greater then c
    private boolean checkGreater(char c, char o){
        if( (o == '*' || o == '/') && (c == '*' || c == '/') ){
            return true;
        } else if( (o == '*' || o == '/') && (c == '+' || c == '-') ){
            return true;
        } else if( (o == '+' || o == '-') && (c == '+' || c == '-') ){
            return true;
        } else{
            return false;
        }
    }
}
