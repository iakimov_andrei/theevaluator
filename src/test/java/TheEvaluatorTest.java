
import com.mycompany.theevaluator.CustomQueue;
import com.mycompany.theevaluator.TheEvaluator;
import com.mycompany.theevaluator.WrongExpressionException;
import java.util.Arrays;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 1331079
 */
@RunWith(Parameterized.class)
public class TheEvaluatorTest {
    /**
     * A static method is required to hold all the data to be tested and the
     * expected results for each test. This data must be stored in a
     * two-dimension array. The 'name' attribute of Parameters is a JUnit 4.11
     * feature
     *
     * @return The list of arrays
     */
   @Parameters(name = "{index} plan[{0}]={1}]")
   public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            {"2+4*3/3", "243*3/+"},
            {"2*2/3", "22*3/"},
            {"3+5+3", "35+3+"},
            {"25*33/15+7", "2533*15/7+"},
            {"5/5*3", "55/3*"},
            {"2+3-6+5", "23+6-5+"},
            {"15-15+1", "1515-1+"},
            {"2*3/3-5", "23*3/5-"},
            {"12+5-4*3", "125+43*-"},
            {"15 + 34 - 6 / 2", "1534+62/-"},
            {"x+4", ""},
            {"wrong exception", ""},
            {"12%5", ""},
            {"15-3/y", ""},
            {"x + y - 3", ""}
        });
    }           
    String in;
    String out;
    
    public TheEvaluatorTest (String in, String out){
        this.in = in;
        this.out = out;
    }               
    
    @Test
    public void Test()
    {
        TheEvaluator ev = new TheEvaluator(in);
        CustomQueue resQ = new CustomQueue();
        String res = "";
        try {
            resQ = ev.toPostfix();           
            while(resQ.size() != 0 ){
                res += resQ.remove();
            }
        } catch (WrongExpressionException ex) {
            Logger.getLogger(TheEvaluatorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertEquals(out, res);
    }
    
}
